## READ ME

** Install **

1. Click **Downloads** on the left side.
2. Click Download repository
3. Open the download files and copy app/ folder
4. Go to your root magento directory and paste merge the app directory.
5. Login to your magento admin.
6. Refresh cache (System->Cache Management)

** To Test **

1. Create a magento backend order at Sales -> Orders -> Create New Order
2. New Payment Method should be one of the payment method option.
3. Submit Order
4. Check created Transaction and Invoice