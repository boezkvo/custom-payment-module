<?php
/**
 * Andy
 *
 * NOTICE OF LICENSE
 *
 * This source file is free.
 *

 * @package    Andy_NewPayment
 * @author     Andy Homalayan <andy.homalayan@gmail.com>
 */
class Andy_NewPayment_Model_Standard extends Mage_Payment_Model_Method_Abstract {

    protected $_code = 'newpayment';
    protected $_canAuthorize = true;
    protected $_canCapture = true;
    protected $_canUseCheckout = false;

    const XML_PATH_NEW_PAYMENT_API_ENDPOINT = 'payment/newpayment/api_endpoint';

    /**
     * Validate payment method information object
     *
     * @param   Mage_Payment_Model_Info $info
     */

    public function validate()
    {
        $paymentInfo = $this->getInfoInstance();
        if ($paymentInfo instanceof Mage_Sales_Model_Order_Payment) {
            $payment = $paymentInfo->getOrder()->getPayment();
        } else {
            $payment = $paymentInfo->getQuote()->getPayment();
        }
        
        $result = $this->apiLookup();
        if (!$result) {
            $errorMsg = $this->_getHelper()->__('There was an error processing your request');
        } else {
            if ($result->status == 'Success') {
                $payment->setTransactionId($result->txn_ref);
                $payment->setIsTransactionClosed(1);
                $payment->setTransactionAdditionalInfo(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS, (array) $result);
            } else {
                Mage::throwException('There was an error processing your request');
            }
        }
        if ($errorMsg) {
            Mage::throwException($errorMsg);
        }
        return $this;
    }

    /**
     * Restful API
     *
     * @return Object
     */

    public function apiLookup() 
    {
        try {
            $quote = $this->getSessionQuote();
            $customerId = $this->getCustomerId();
            $url = Mage::getStoreConfig(self::XML_PATH_NEW_PAYMENT_API_ENDPOINT);
            $ch = curl_init($url);
            $jsonData = array(
                'amount' => $quote->getGrandTotal(),
                'customer_id' =>  $customerId,
                'reference' => $quote->getId()
            );
            
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($jsonData));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
            $result = json_decode(curl_exec($ch));
            return $result;
        }
        catch (Exception $e) {
            Mage::throwException($e->getMessage());
        }
    }

    /**
     * Get Select Admin Customer ID
     *
     * @return Integer
     */

    protected function getCustomerId()
    {
        if (Mage::app()->getStore()->isAdmin()) {
            return Mage::getSingleton('adminhtml/sales_order_create')->getQuote()->getCustomerId();
        }

        return null;
    }

    /**
     * Get Admin Quote Session Object
     *
     * @return Object
     */

    protected function getSessionQuote()
    {
        // Backend Order
        if (Mage::app()->getStore()->isAdmin())
        {
            return Mage::getSingleton('adminhtml/sales_order_create')->getQuote();
        }
        // Frontend but unused
        return Mage::getSingleton('checkout/session')->getQuote();
    }

}